//
//  MarvelTests.swift
//  MarvelTests
//
//  Created by Jan Cássio on 4/14/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import XCTest
import Alamofire

@testable import Marvel

class MarvelClientTests: XCTestCase {
  
  var client: MarvelClient?
  
  let apiKey: String = "0ff6a03ba97ea28e057ac72cdfb6b5cd"
  
  override func setUp() {
    super.setUp()
    client = MarvelClient()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testAuthenticationHash () {
    let ts: String = "1"
    
    let result: String = client!.md5(timestamp: ts)
    let expected: String = "faebaac3f732b98d5e85926841eadb65"
    
    XCTAssertTrue(result == expected, "MD5 generated is \(result) expected \(expected)")
  }
  
  func testAuthParams () {
    let params: [ String: String ] = client!.authParams(timestamp: "1")
    
    let expectedHash: String = "faebaac3f732b98d5e85926841eadb65"
    let expectedTs: String = "1"
    let expectedApiKey = apiKey
    
    XCTAssertEqual(params["hash"], expectedHash, "Hash generated is \(params["hash"]) expected \(expectedHash)")
    XCTAssertEqual(params["ts"], expectedTs, "Ts generated is \(params["ts"]) expected \(expectedTs)")
    XCTAssertEqual(params["apikey"], expectedApiKey, "Api Key generated is \(params["apikey"]) expected \(expectedApiKey)")
  }
  
  func testURLForCharactersRequest () {
    let url = client?.urlForService("characters/")
    let expected = "http://gateway.marvel.com/v1/public/characters/"
    
    XCTAssertEqual(url?.absoluteString, expected, "URL generated \(url?.absoluteString) expected \(expected)")
  }
  
  // TODO: - Improve this test using NSURL comparison (specially query & path values)
  func testURLWithAuthParamsForCharactersRequest () {
    let url = client!.urlForService("characters/", params: client!.authParams(timestamp: "1"))
    
    let expected = NSURL(string:"http://gateway.marvel.com/v1/public/characters/?hash=faebaac3f732b98d5e85926841eadb65&apiKey=0ff6a03ba97ea28e057ac72cdfb6b5cd&ts=1")!
    
    XCTAssertEqual(url.absoluteString, expected.absoluteString, "URL received \(url.absoluteString) expected \(expected.absoluteString)")
  }
  
  func testCharRequestHasNoErrorResponse () {
    let expectation = expectationWithDescription("Characters request")
    
    client?.characters() { (result, error) in
      XCTAssertNil(error)
      expectation.fulfill()
    }
    
    waitForExpectationsWithTimeout(10) {error in
      if let _ = error {
        print("Error: \(error!.localizedDescription)")
      }
    }
  }
}
