//
//  Alerts.swift
//  Marvel
//
//  Created by Jan Cássio on 4/17/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit

struct Alerts {
  static func simpleAlertWith (title: String, message: String) -> UIAlertController {
    let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
    
    alert.addAction(UIAlertAction(
      title: i18n("OK"),
      style: .Cancel,
      handler: nil
    ))
    
    return alert
  }
}