//
//  RefreshBottomTableViewCell.swift
//  Marvel
//
//  Created by Jan Cássio on 4/16/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit

class RefreshBottomTableViewCell: UITableViewCell {
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
  
  override func prepareForReuse() {
    activityIndicator?.startAnimating()
  }
}
