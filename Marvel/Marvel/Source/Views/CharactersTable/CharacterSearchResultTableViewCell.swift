//
//  CharacterSearchResultTableViewCell.swift
//  Marvel
//
//  Created by Jan Cássio on 4/19/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CharacterSearchResultTableViewCell: UITableViewCell {

  static let reusableId = Resources.Interfaces.SearchResultCell.rawValue
  
  @IBOutlet weak var thumbView: UIImageView?
  @IBOutlet weak var label: UILabel?
  
  let api: MarvelClient = MarvelClient()
  var topBorder: CAShapeLayer?

  var title: String? {
    didSet {
      self.label!.text = self.title
    }
  }
  
  var thumbnail: MarvelCharacterModel.Thumbnail? {
    didSet {
      if
        let thumbnail = self.thumbnail,
        let path = thumbnail.path {
        let url: NSURL = self.api.urlForImage(
          path,
          ext: thumbnail.ext,
          size: .Square
        )!
        
        thumbView!.af_setImageWithURL(url)
      }
    }
  }
  
  
  override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
  }

  override func setSelected(selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)

      // Configure the view for the selected state
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    guard topBorder == nil else { return }
    drawTopBorder(bounds)
  }
  
  func drawTopBorder (rect: CGRect) {
    let path: UIBezierPath = UIBezierPath(
      rect: CGRectMake(0, 0, rect.width, 1)
    )
    
    topBorder = CAShapeLayer()
    topBorder?.path = path.CGPath
    topBorder?.fillColor = Resources.Colors.Secondary_Dark.color().CGColor
    
    layer.addSublayer(topBorder!)
  }

}
