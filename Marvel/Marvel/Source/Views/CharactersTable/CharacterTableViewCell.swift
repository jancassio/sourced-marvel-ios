//
//  CharacterTableViewCell.swift
//  Marvel
//
//  Created by Jan Cássio on 4/18/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SweetConsole

class CharacterTableViewCell: UITableViewCell {
  
  static let reusableId = Resources.Interfaces.CharacterCell.rawValue
  
  let api: MarvelClient = MarvelClient()
  
  @IBOutlet weak var label: UILabel?
  @IBOutlet weak var characterImage: UIImageView?
  @IBOutlet weak var imageWrapper: UIView?
  
  var labelText: String? {
    didSet {
      self.label!.text = self.labelText!
      self.label?.sizeToFit()
    }
  }
  
  var thumbnail: MarvelCharacterModel.Thumbnail? {
    didSet {
      if
        let thumbnail = self.thumbnail,
        let path = thumbnail.path {
        let url: NSURL = self.api.urlForImage(
          path,
          ext: thumbnail.ext,
          size: .Landscape
        )!
        
        let placeholder: UIImage = Resources.Images.IC_Nav_Marvel.image()!
        self.characterImage!.af_setImageWithURL(
          url,
          placeholderImage: placeholder
        )
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    imageWrapper?.clipsToBounds = true
  }

  func updateScrollOffset (offsetY: CGFloat) {
    let h: CGFloat = CGRectGetHeight(characterImage!.bounds)
    let cX: CGFloat = CGRectGetMidX(imageWrapper!.bounds)
    let y: CGFloat = CGRectGetMidY(imageWrapper!.bounds)
    let d: CGFloat = (h - CGRectGetHeight(frame)) * 0.25
    let r: CGFloat = (offsetY - frame.origin.y) / h
    let cY: CGFloat = y + (r * d)
    
    characterImage?.center = CGPointMake(cX, cY)
  }
}
