//
//  SearchResultsViewController.swift
//  Marvel
//
//  Created by Jan Cássio on 4/19/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit

class SearchResultsViewController: BaseTableViewController {

  var results: [ MarvelCharacterModel ]? {
    didSet {
      rows = results!.count
      tableView.reloadData()
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.registerNib(
      Resources.Interfaces.SearchResultCell.UInib(),
      forCellReuseIdentifier: CharacterSearchResultTableViewCell.reusableId
    )
    
    tableView.separatorStyle = .None
    tableView.backgroundColor = Resources.Colors.Secondary_Dark.color()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: CharacterSearchResultTableViewCell = tableView.dequeueReusableCellWithIdentifier(CharacterSearchResultTableViewCell.reusableId) as! CharacterSearchResultTableViewCell
    
    let data: MarvelCharacterModel = results![ indexPath.row ]
    cell.title = data.name
    cell.thumbnail = data.thumbnail
    
    return cell
  }
}
