//
//  CharactersTableViewController.swift
//  Marvel
//
//  Created by Jan Cássio on 4/16/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit
import SweetConsole

class CharactersTableViewController: BaseTableViewController {

  // MARK: - Properties
  
  // MARK: Static const
  
  static let reusableId = Resources.Interfaces.RefreshBottomCell.rawValue
  
  // MARK: instance variables
  var provider: MarvelCharactersProvider = MarvelCharactersProvider()
  var searchController: UISearchController?
  var searchResultsTable: SearchResultsViewController = SearchResultsViewController()
  
  // MARK: Lazy variables
  lazy var fetchHanlder: MarvelCharactersProvider.Response = { [weak self] (result, error) in
    guard error == nil else {
      Alerts.simpleAlertWith(i18n("error"), message: error!.localizedDescription)
      return
    }
    
    self!.rows = result!.count + 1
    self!.tableView.reloadData()
    self!.updateCellBackground()
  }
  
  
  
  // MARK: - Setup
  
  func setupNavigationBar () {
    let titleImage: UIImage = Resources.Images.IC_Nav_Marvel.image()!
    let searchIcon: UIImage = Resources.Images.IC_Nav_Search.image()!
    
    let title: UIImageView = UIImageView(image: titleImage)
    navigationItem.titleView = title
    
    let searchButton: UIBarButtonItem = UIBarButtonItem(image: searchIcon, style: .Plain, target: self, action: #selector(CharactersTableViewController.onSearchTouch))
    searchButton.tintColor = Resources.Colors.Primary_Red.color()
    
    navigationItem.rightBarButtonItem = searchButton
  }
  
  func setupTableView () {
    tableView.registerNib(
      Resources.Interfaces.RefreshBottomCell.UInib(),
      forCellReuseIdentifier: CharactersTableViewController.reusableId
    )
    
    tableView.registerNib(
      Resources.Interfaces.CharacterCell.UInib(),
      forCellReuseIdentifier: CharacterTableViewCell.reusableId
    )
    
    tableView.separatorStyle = .None
  }
  
  func setupSearchController () {
    let darkColor: UIColor = Resources.Colors.Primary_Dark.color()
    let redColor: UIColor = Resources.Colors.Primary_Red.color()
    
    UISearchBar.appearance().barTintColor = darkColor
    UISearchBar.appearance().tintColor = redColor
    UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.self]).tintColor = darkColor
    
    searchController = UISearchController(searchResultsController: searchResultsTable)
    searchController!.searchResultsUpdater = self
    searchController!.searchBar.sizeToFit()
    tableView.tableHeaderView = searchController?.searchBar
    
    searchController!.dimsBackgroundDuringPresentation = false
    definesPresentationContext = true
//    tableView.tableHeaderView = searchController.searchBar
  }
  
  // MARK: - View Phases
  
  override func viewDidLoad() {
      super.viewDidLoad()
    
    setupNavigationBar()
    setupTableView()
    setupSearchController()
  }

  override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    fetchHead()
    updateCellBackground()
  }

  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if indexPath.row < provider.data.count {
      return characterCellForIndexPath(indexPath)
    }
    else {
      return refreshBottonCell()
    }
  }
  
  override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if cell.isKindOfClass( RefreshBottomTableViewCell ) {
      fetchTail()
    }
  }
  
  
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.row < provider.data.count {
      return 150.0
    }
    else {
      return 44.0
    }
  }
  
  override func scrollViewDidScroll(scrollView: UIScrollView) {
    updateCellBackground()
  }
  
  
  
  // MARK: Table utils
  func characterCellForIndexPath (indexPath: NSIndexPath) -> UITableViewCell {
    let cell: CharacterTableViewCell = tableView.dequeueReusableCellWithIdentifier(CharacterTableViewCell.reusableId, forIndexPath: indexPath) as! CharacterTableViewCell
    
    let marvelChar: MarvelCharacterModel = provider.data[ indexPath.row ]
    
    cell.labelText = marvelChar.name
    cell.thumbnail = marvelChar.thumbnail!
    
    return cell
  }
  
  func refreshBottonCell () -> UITableViewCell {
    return tableView.dequeueReusableCellWithIdentifier(
      CharactersTableViewController.reusableId
    )!
  }
  
  func updateCellBackground () {
    let offsetY = self.tableView.contentOffset.y
    
    for cell in tableView.visibleCells {
      if let cell: CharacterTableViewCell = cell as? CharacterTableViewCell {
        cell.updateScrollOffset(offsetY)
      }
    }
  }

  /*
  // MARK: - Navigation

  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
  }
  */
  
  
  // MARK: - API interaction 
  
  func fetchHead () {
    provider.fetch(1, response: fetchHanlder)
  }
  
  func fetchTail () {
    guard !searchController!.active && provider.data.count > 0 else {
      return
    }
    
    provider.fetchTail(fetchHanlder)
  }
  
  
  // MARK: - UI Action handlers 
  
  func onSearchTouch () {
    searchController!.active = !searchController!.active
  }
}

extension CharactersTableViewController: UISearchResultsUpdating {
  func updateSearchResultsForSearchController(searchController: UISearchController) {
    searchResultsTable.results = provider.filterDataWith(searchController.searchBar.text)
  }
}