//
//  BaseNavigationController.swift
//  Marvel
//
//  Created by Jan Cássio on 4/17/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return .LightContent
  }
}