//
//  BaseTableViewController.swift
//  Marvel
//
//  Created by Jan Cássio on 4/19/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {
  
  var rows: Int = 0
  var sessions: Int = 1
  
  override func viewDidLoad() {
      super.viewDidLoad()
  }

  override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
  }

  // MARK: - Table view data source

  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
      // #warning Incomplete implementation, return the number of sections
      return sessions
  }

  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      // #warning Incomplete implementation, return the number of rows
      return rows
  }
}
