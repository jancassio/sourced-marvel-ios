//
//  MarvelCharacterModel.swift
//  Marvel
//
//  Created by Jan Cássio on 4/15/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import Foundation
import ObjectMapper

struct MarvelCharacterModel: Mappable {
  
  var id: Int = 0
  var name: String = ""
  var description: String = ""
  var modified: NSDate?
  var resourceURI: NSURL?
  var thumbnail: Thumbnail?
  var comics: Comics?
  var series: Series?
  var stories: Stories?
  var events: Events?
  
  init? (_ :Map) {
    
  }
  
  mutating func mapping(map: Map) {
    id          <- map["id"]
    name        <- map["name"]
    /*
      TODO: The default date tranformmer of ObjectMapper,
      can't transform Marvel's date format correctly.
      Needed create a custom date transformer for that.
    */
    modified    <- (map["modified"], DateTransform())
    description <- map["description"]
    resourceURI <- (map["resourceURI"], URLTransform())
    thumbnail   <- map["thumbnail"]
    comics      <- map["comics"]
    series      <- map["series"]
    stories     <- map["stories"]
  }
  
  struct Thumbnail: Mappable {
    
    var path: NSURL?
    var ext: String = ""
    
    init? (_ :Map) {
      
    }
    
    mutating func mapping(map: Map) {
      path  <- (map["path"], URLTransform())
      ext   <- map["extension"]
    }
  }
  
  struct Comics: Mappable {
    var available: Int = 0
    var collectionURI: NSURL?
    var returned: Int = 0
    var items: [ Item ] = []
    
    init? (_ :Map) {
      
    }
    
    mutating func mapping(map: Map) {
      available     <- map["available"]
      collectionURI <- map["collectionURI"]
      returned      <- map["returned"]
      items         <- map["items"]
    }
    
    struct Item: Mappable {
      var name: String = ""
      var resourceURI: NSURL?
      
      init? (_ :Map) {
        
      }
      
      mutating func mapping(map: Map) {
        name <- map["name"]
        resourceURI <- (map["resourceURI"], URLTransform())
      }
    }
  }
  
  struct Series : Mappable {
    var available: Int = 0
    var collectionURI: NSURL?
    var items: [ Item ] = []
    
    init? (_ :Map) {
      
    }
    
    mutating func mapping(map: Map) {
      available     <- map["available"]
      collectionURI <- (map["collectionURI"], URLTransform())
      items         <- map["items"]
    }
    
    struct Item : Mappable {
      var resourceURI: NSURL?
      var name: String = ""
      
      init? (_ :Map) {
        
      }
      
      mutating func mapping(map: Map) {
        resourceURI <- (map["resourceURI"], URLTransform())
        name <- map["name"]
      }
    }
  }
  
  struct Stories : Mappable {
    var available: Int = 0
    var collectionURI: NSURL?
    var returned: Int = 0
    var items: [ Item ] = []
    
    init? (_ :Map) {
      
    }
    
    mutating func mapping(map: Map) {
      available     <- map["available"]
      collectionURI <- (map["collectionURI"], URLTransform())
      returned      <- map["returned"]
      items         <- map["items"]
    }
    
    struct Item : Mappable {
      var resourceURI: NSURL?
      var name: String = ""
      var type: String = ""
      
      init? (_ :Map) {
        
      }
      
      mutating func mapping(map: Map) {
        resourceURI <- (map["resourceURI"], URLTransform())
        name        <- map["name"]
        type        <- map["type"]
      }
    }
  }
  
  struct Events : Mappable {
    var available: Int = 0
    var collectionURI: NSURL?
    var items: [ Item ] = []
    
    init? (_ :Map) {
      
    }
    
    mutating func mapping(map: Map) {
      available       <- map["available"]
      collectionURI   <- (map["collectionURI"], URLTransform())
      items           <- map["items"]
    }
    
    struct Item: Mappable {
      var name: String = ""
      var resourceURI: NSURL?
      
      init?(_ :Map) {
        
      }
      
      mutating func mapping(map: Map) {
        name        <- map["name"]
        resourceURI <- (map["resourceURI"], URLTransform())
      }
    }
  }
  
  struct URL : Mappable {
    var type: String = ""
    var url: NSURL?
    
    init? (_ :Map) {
      
    }
    
    mutating func mapping(map: Map) {
      type  <- map["type"]
      url   <- (map["url"], URLTransform())
    }
  }
}
