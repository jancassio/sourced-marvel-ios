//
//  MarvelCharactersResponseModel.swift
//  Marvel
//
//  Created by Jan Cássio on 4/15/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import Foundation
import ObjectMapper

struct MarvelCharactersResponseModel: Mappable {
  var offset: Int = 0
  var limit: Int = 0
  var total: Int = 0
  var count: Int = 0
  var results: [ MarvelCharacterModel ] = []
  
  init?(_ map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    offset  <- map["offset"]
    limit   <- map["limit"]
    total   <- map["total"]
    count   <- map["count"]
    results <- map["results"]
  }
}