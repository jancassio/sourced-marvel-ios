//
//  MarvelResponseModel.swift
//  Marvel
//
//  Created by Jan Cássio on 4/15/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import Foundation
import ObjectMapper

struct MarvelResponseModel<T: Mappable>: Mappable {
  
  var code: Int = 0
  var status: String = ""
  var copyright: String = ""
  var attributionText: String = ""
  var etag: String = ""
  var data: T?
  
  init?(_ map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    code            <- map["code"]
    status          <- map["status"]
    copyright       <- map["copyright"]
    attributionText <- map["attributionText"]
    etag            <- map["etag"]
    data            <- map["data"]
  }
}