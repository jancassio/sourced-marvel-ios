//
//  Colors.swift
//  Marvel
//
//  Created by Jan Cássio on 4/16/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit

extension UIColor {
  convenience init(hex: Int, alpha: CGFloat = 1.0) {
    let red   = CGFloat( (hex & 0xFF0000) >> 16 ) / 255.0
    let green = CGFloat( (hex & 0xFF00) >> 8 ) / 255.0
    let blue  = CGFloat( (hex & 0xFF) ) / 255.0
    
    self.init(red:red, green:green, blue:blue, alpha:alpha)
  }
  
  /**
   Convenience method to create color with RGB range 0-255 and alpha 0-100
   */
  convenience init (r: Int, g: Int, b: Int, a: Int = 100 ) {
    self.init(
      red: CGFloat(r) / 255.0,
      green: CGFloat(g) / 255.0,
      blue: CGFloat(b) / 255.0,
      alpha: CGFloat(a) / 100.0
    )
  }
}