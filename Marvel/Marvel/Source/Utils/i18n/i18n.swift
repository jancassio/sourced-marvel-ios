//
//  i18n.swift
//  Marvel
//
//  Created by Jan Cássio on 4/16/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import Foundation

func i18n (key: String) -> String {
  return NSLocalizedString(key, comment: "")
}