//
//  ErrorCodes.swift
//  Marvel
//
//  Created by Jan Cássio on 4/16/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import Foundation

enum ErrorCode: Int {
  case ParseFailure = 1001
  
  var description: String {
    switch self {
    case .ParseFailure:
      return "error:parsing"
    }
  }
}