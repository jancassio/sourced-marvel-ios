//
//  ErrorDomains.swift
//  Marvel
//
//  Created by Jan Cássio on 4/16/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import Foundation

enum ErrorDomains: String {
  case Application = "com.jancassio.marvel.Marvel:ApplicationErrorDomain"
}