//
//  ErrorFactory.swift
//  Marvel
//
//  Created by Jan Cássio on 4/16/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import Foundation

final class ErrorFactory {
  
  class func create (code: ErrorCode) -> NSError {
    return NSError(
      domain: ErrorDomains.Application.rawValue,
      code: code.rawValue,
      userInfo: [
        NSLocalizedDescriptionKey: code.description
      ]
    )
  }
}