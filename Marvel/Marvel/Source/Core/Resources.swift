//
//  Resources.swift
//  Marvel
//
//  Created by Jan Cássio on 4/17/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import UIKit

protocol InterfaceResource {
  var rawValue: String { get }
  func interface <T>() -> T
  func name () -> String
  func UInib () -> UINib
}

protocol ColorResource {
  var rawValue: Int { get }
  func color (alpha: CGFloat) -> UIColor
}

protocol FontResource {
  var rawValue: String { get }
  func font (size: CGFloat) -> UIFont?
}

protocol ImageResource {
  var rawValue: String { get }
  func image () -> UIImage?
}

extension InterfaceResource {
  func interface <T>() -> T {
    return NSBundle.mainBundle().loadNibNamed(name(), owner: nil, options: nil).first as! T
  }
  
  func name () -> String {
    return "\(rawValue)Interface"
  }
  
  func UInib () -> UINib {
    return UINib(nibName: name(), bundle: nil)
  }
}

extension ColorResource {
  func color (alpha: CGFloat = 1.0) -> UIColor {
    return UIColor(hex: rawValue, alpha: alpha)
  }
}

extension FontResource {
  func font (size: CGFloat = 12.0) -> UIFont? {
    return UIFont(name: rawValue, size: size)
  }
}

extension ImageResource {
  func image () -> UIImage? {
    return UIImage(named: rawValue)
  }
}


struct Resources {
  
  enum Images: String, ImageResource {
    case BG_Cell_Title      = "bg-cell-title"
    case IC_Cell_Disclosure = "icn-cell-disclosure"
    case IC_Nav_BlackWhite  = "icn-nav-back-white"
    case IC_Nav_Close_White = "icn-nav-close-white"
    case IC_Nav_Marvel      = "icn-nav-marvel"
    case IC_Nav_Search      = "icn-nav-search"
  }
  
  enum Colors: Int, ColorResource {
    case Primary_Red = 0xf01023
    case Primary_Dark = 0x0c0e0f
    case Secondary_Dark = 0x3c3f43
  }
  
  enum Interfaces: String, InterfaceResource {
    case RefreshBottomCell = "RefreshBottomTableViewCell"
    case CharacterCell     = "CharacterTableViewCell"
    case SearchResultCell  = "SearchResultTableViewCell"
  }
  
}