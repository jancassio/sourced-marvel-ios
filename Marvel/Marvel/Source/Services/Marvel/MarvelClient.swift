//
//  MarvelClient.swift
//  Marvel
//
//  Created by Jan Cássio on 4/14/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SweetHMAC

class MarvelClient {
  
  enum ImageSize: String {
    case Landscape = "landscape_incredible"
    case Square = "standard_large"
  }
  
  typealias CharactersResponse =
    (MarvelResponseModel<MarvelCharactersResponseModel>?, error: NSError?) -> Void
  // MARK: - Constants
  
  private let endpoint: NSURL     = NSURL(string:"http://gateway.marvel.com/v1/public/")!
  private let apiKey: String      = "0ff6a03ba97ea28e057ac72cdfb6b5cd"
  private let apiSecret: String   = "a72a77257f90843d0d0fc12b97a1c4357bffd536"
  
  
  
  
  // MARK: - Getters
  
  var baseURLComponent: NSURLComponents {
    return NSURLComponents(URL: self.endpoint, resolvingAgainstBaseURL: true)!
  }
  
  var timestamp: String {
    return "\( NSDate().timeIntervalSince1970 * 1000 )"
  }
  
  
  
  
  // MARK: - API Methods
  func characters (offset: Int, responseBlock: CharactersResponse) {
    var params = authParams(timestamp: timestamp)
    params["offset"] = offset
    
    let requestURL: NSURL = urlForService(
      "characters",
      params: params
    )
    
    Alamofire.request(.GET, requestURL)
      .responseJSON { response in
        switch response.result {
        case .Success:
          if let model: MarvelResponseModel<MarvelCharactersResponseModel> =
            Mapper<MarvelResponseModel
              <MarvelCharactersResponseModel>
              >().map(response.result.value) {
            responseBlock(model, error: nil)
          }
          else {
            let error = ErrorFactory.create(.ParseFailure)
            responseBlock(nil, error: error)
          }
          
          break
          
        case .Failure(let error):
          responseBlock(nil, error: error)
          break
        }
    }
  }
  
  
  
  // MARK: - URL composition utils
  
  func urlForService (serviceName: String) -> NSURL {
    return NSURL(string: "\(serviceName)", relativeToURL: baseURLComponent.URL!)!
  }
  
  func urlForImage (uri: NSURL, ext: String, size: ImageSize) -> NSURL? {
    return uri.URLByAppendingPathComponent("/\(size.rawValue).\(ext)")
  }
  
  func urlForService (serviceName: String, params: [String: AnyObject]) -> NSURL {
    let baseURL: NSURL = urlForService(serviceName)
    let urlWithParams: NSURLComponents =  NSURLComponents(URL: baseURL, resolvingAgainstBaseURL: true)!
    
    urlWithParams.queryItems = query(params)
    
    return urlWithParams.URL!
  }
  
  func md5 (timestamp ts: String) -> String {
      return "\(ts + apiSecret + apiKey)".MD5()
  }
  
  func query (params: [ String: AnyObject ]) -> [ NSURLQueryItem ] {
    var q: [ NSURLQueryItem ] = []
    
    for (name, value) in params {
      q.append( NSURLQueryItem(name: name, value: "\(value)") )
    }
    
    return q;
  }
  
  func authParams (timestamp ts: String) -> [ String: AnyObject ] {
    return [
      "apikey": apiKey,
      "ts": ts,
      "hash": md5(timestamp: ts)
    ]
  }
}