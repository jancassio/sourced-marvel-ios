//
//  MarvelCharactersProvider.swift
//  Marvel
//
//  Created by Jan Cássio on 4/17/16.
//  Copyright © 2016 Jan Cassio. All rights reserved.
//

import Foundation

class MarvelCharactersProvider {
  typealias Response = (result:[MarvelCharacterModel]? , error: NSError?) -> Void
  let api: MarvelClient = MarvelClient()
  let limit: Int = 20
  
  var currentPage: Int = 0
  var isFetching: Bool = false
  
  private var _results: [ MarvelCharacterModel ] = []
  
  var data: [ MarvelCharacterModel ] {
    return self._results;
  }
  
  func fetch (page: Int = 1, response responseBlock: Response) {
    guard !isFetching else {
      return
    }
    
    isFetching = true
    
    let offset = (page - 1) * limit
    api.characters(offset) { (response, error) in
      guard error == nil else {
        responseBlock(result: nil, error: error)
        return
      }
      
      if self._results.count < (offset + self.limit) {
        self._results.appendContentsOf(response!.data!.results)
      }
      
      responseBlock(result: self._results, error: nil)
      self.isFetching = false
    }
  }
  
  func fetchTail (response: Response) {
    currentPage = currentPage + 1
    fetch(currentPage, response: response)
  }
  
  func filterDataWith (text: String?) -> [ MarvelCharacterModel ] {
    if let text = text {
      return _results.filter({ model -> Bool in
        return model.name.lowercaseString.containsString(text.lowercaseString)
      })
    }
    
    return []
  }
}