//: Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"

var urlString: String = "http://www.google.com"
var url: NSURL = NSURL(string:urlString)!
var urlComponent: NSURLComponents = NSURLComponents(URL: url, resolvingAgainstBaseURL: true)!
var path = "q/flksdjfkl"

var newURL = NSURL(string: path, relativeToURL: urlComponent.URL)!.absoluteString

var r = urlComponent.URL?.absoluteString

